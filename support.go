package main

import "encoding/xml"

const EXCELSA_CATALOG = "ec806200-6432-4973-a5eb-41001076dc4b"
const LAVAZZA_CATALOG = "3b5e048c-80bc-4e96-8338-a255007da88e"

type KommInfo struct {
	XMLName       xml.Name `xml:"КоммерческаяИнформация"`
	Text          string   `xml:",chardata"`
	Xmlns         string   `xml:"xmlns,attr"`
	Xs            string   `xml:"xs,attr"`
	Xsi           string   `xml:"xsi,attr"`
	Version       string   `xml:"ВерсияСхемы,attr"`
	DateCreated   string   `xml:"ДатаФормирования,attr"`
	Id            string   `xml:"Ид,attr"`
	OffersPackage struct {
		Text            string `xml:",chardata"`
		HasOnlyDIffs    string `xml:"СодержитТолькоИзменения,attr"`
		Id              string `xml:"Ид"`
		Name            string `xml:"Наименование"`
		CatalogId       string `xml:"ИдКаталога"`
		ClassificatorId string `xml:"ИдКлассификатора"`
		Offers          struct {
			Text  string `xml:",chardata"`
			Offer []struct {
				Text   string `xml:",chardata"`
				Id     string `xml:"Ид"`
				Prices struct {
					Text  string `xml:",chardata"`
					Price []struct {
						Text        string `xml:",chardata"`
						DisplayText string `xml:"Представление"`
						PriceTypeId string `xml:"ИдТипаЦены"`
						PricePerOne string `xml:"ЦенаЗаЕдиницу"`
						Currency    string `xml:"Валюта"`
						Tax         struct {
							Text           string `xml:",chardata"`
							Name           string `xml:"Наименование"`
							IsAppliedToSum string `xml:"УчтеноВСумме"`
						} `xml:"Налог"`
					} `xml:"Цена"`
				} `xml:"Цены"`
			} `xml:"Предложение"`
		} `xml:"Предложения"`
	} `xml:"ПакетПредложений"`
}
