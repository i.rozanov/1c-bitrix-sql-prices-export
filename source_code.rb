﻿# encoding: UTF-8
require 'nokogiri'
# require 'mini_portile'
require 'io/console'  
require 'clipboard'
require "ffi"
puts "Выполняется поиск файлов xml..."
flnm= Dir["price*.xml"][0]
puts "Найден файл "+ flnm
xmlfile = File.new(flnm)
doc = Nokogiri::XML(xmlfile)
puts "В данном файле найдено " + doc.css('ЦенаЗаЕдиницу').count.to_s + " цен."
num=0
main_catalog_id = doc.css('ПакетПредложений > Ид').text.chomp('#')
if main_catalog_id=='3b5e048c-80bc-4e96-8338-a255007da88e'
	puts "Это каталог Lavazza!"
	c_id=9000
elsif main_catalog_id=='ec806200-6432-4973-a5eb-41001076dc4b'
	puts "Это каталог Excelsa!"
	c_id=8000
else
	print "Введите id (8 или 9): "
	c_id=gets.chomp.to_i*1000
end
puts "Начальный id: " + (c_id+1).to_s
File.delete('sql.txt') if File.exist?('sql.txt')
clip = ""
clip += "Delete from b_catalog_price where id > " + c_id.to_s + " and id < "+ (c_id+1000).to_s + ";\n"
File.open('sql.txt', 'a') { |file| file.write("Delete from b_catalog_price where id > " + c_id.to_s + " and id < "+ (c_id+1000).to_s + ";\n") }

puts "Данные выгружаются..."
doc.css('Предложение').each do |el|
	element_id =  el.css('Ид').text
	el.css('Цена').each do |pr|
		c_id+=1
		num+=1
		price_xml_id=pr.css('ИдТипаЦены').text
		price = pr.css('ЦенаЗаЕдиницу').text
		sql_str =	"insert into b_catalog_price values (" + c_id.to_s + 
					", (select id from b_iblock_element where xml_id = '" + element_id +
					"' and iblock_id=(select id from b_iblock where xml_id = '" + main_catalog_id + "')"  + 
					"), NULL, (select id from b_catalog_group where xml_id = '" + price_xml_id + "'), " + price + 
					", 'RUB', CURRENT_TIMESTAMP, NULL, NULL, NULL, " + price + ");\n"
		clip+= sql_str
		File.open('sql.txt', 'a') { |file| file.write(sql_str) }
	end
end
sql_str = "UPDATE b_catalog_price t1 inner join (select s.id, s.price, s.product_id  from b_catalog_price s where product_id = (SELECT product_id from b_catalog_price where id = 999999) and CATALOG_GROUP_ID = 2 limit 1) t2 on 1=1 SET t1.price = t2.price, t1.price_scale = t2.price, t1.TIMESTAMP_X = now(), t1.product_id = t2.product_id  where t1.ID = 999999;\n"
clip+= sql_str
File.open('sql.txt', 'a') { |file| file.write(sql_str) }
sql_str = "UPDATE b_catalog_price t1 inner join (select s.price, s.product_id from b_catalog_price s where product_id = (SELECT product_id from b_catalog_price where id = 999999) and CATALOG_GROUP_ID = 3 limit 1) t2 ON t1.product_id=t2.product_id SET t1.price = t2.price where t1.CATALOG_GROUP_ID = 2;\n"
clip+= sql_str
File.open('sql.txt', 'a') { |file| file.write(sql_str) }
Clipboard.copy(clip)
puts '╔' + '═'*21 + '╗'
puts "║  " + num.to_s + " цен выгружено!"  + " ║"
puts '╚' + '═'*21 + '╝'
puts "Данные скопированы в буфер обмена!\n"
print "Нажмите любую клавишу для завершения...\r"
STDIN.getch
print "                                        \r"
# file_to_open = "sql.txt"
# system %{cmd /c "start #{file_to_open}"}
#insert into b_catalog_price values (9001, (select id from b_iblock_element where xml_id like '967931a3-c0b6-11e2-bf17-f0bf976aa6cb' and iblock_id=41), NULL, 3, 777, 'RUB', CURRENT_TIMESTAMP, NULL, NULL, NULL, 777);

#page = doc.at('ПакетПредложений')
#p page.at('Предложения').text
#page.xpath("//Предложение").each do |element|
#	p element
  #p element.xpath('Ид').text
#  count=1

#end


#page['Title'] # => "System Slots"
#page.at('item[@Property="Current Usage"]')['Value'] # => "Available"


#xmldoc = Document.new(xmlfile)
#xmldoc.elements.each("КоммерческаяИнформация/ПакетПредложений/Предложения/Предложение"){ |e|
#p xmldoc.elements.each("КоммерческаяИнформация")

#XPath.each(doc, "//Предложение") { |e| 
	#p e
#}

#	p e
#}
# print "Введите id каталога (41-exc, 78-lav): "
# cat_id=gets.chomp
# puts cat_id
