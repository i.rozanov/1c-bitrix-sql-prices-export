package main

import (
	"encoding/xml"
	"fmt"
	"golang.design/x/clipboard"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func main() {
	// Find the file
	directory := getCurrentDirectory()
	pricesFilePath := findLastFile(directory)
	fmt.Println(pricesFilePath)
	// parse it
	xmlContent := getXmlContents(pricesFilePath)
	kommInfo := parseXML(xmlContent)
	// processing
	printNumberOfPrices(kommInfo)
	catalogId := getCatalogType(kommInfo)
	if catalogId > 0 {
		sqlCommands := makeSqlCommands(kommInfo, catalogId)
		saveToFile(sqlCommands)
		saveToClipboard(sqlCommands)
		fmt.Println("Успешно выгружено!")
	}
}

func saveToClipboard(sql string) {
	err := clipboard.Init()
	if err != nil {
		panic(err)
	}
	clipboard.Write(clipboard.FmtText, []byte(sql))
}

func saveToFile(sql string) {
	err := os.WriteFile("sql.txt", []byte(sql), 0644)
	if err != nil {
		panic(err)
	}
}

func getCurrentDirectory() string {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	return exPath
}

func findLastFile(rootPath string) string {
	lastFile := ""
	lastModTime := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	var r, _ = regexp.Compile("prices.*.xml")
	err := filepath.Walk(rootPath, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if r.MatchString(filepath.Base(path)) && info.ModTime().After(lastModTime) {
			lastFile = path
			lastModTime = info.ModTime()
		}
		return nil
	})
	if err != nil {
		fmt.Printf("walk error [%v]\n", err)
	}
	return lastFile
}

func getXmlContents(path string) string {
	xmlFile, err := os.ReadFile(path)

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Файл успешно открывается")

	return string(xmlFile)
}

func parseXML(xmlContent string) *KommInfo {
	kommInfo := new(KommInfo)
	err := xml.Unmarshal([]byte(xmlContent), kommInfo)
	if err != nil {
		fmt.Printf("error: %v", err)
		return nil
	}
	return kommInfo
}

func printNumberOfPrices(kommInfo *KommInfo) {
	qty := 0
	for _, offer := range kommInfo.OffersPackage.Offers.Offer {
		for _, price := range offer.Prices.Price {
			if len(price.PricePerOne) > 0 {
				qty++
			}
		}
	}
	fmt.Printf("Было найдено %v цен\n", qty)
}

func getCatalogType(kommInfo *KommInfo) int {
	kommInfo.OffersPackage.Id = strings.TrimRight(kommInfo.OffersPackage.Id, "#")
	fmt.Println(kommInfo.OffersPackage.Id)
	switch kommInfo.OffersPackage.Id {
	case LAVAZZA_CATALOG:
		fmt.Println("Это каталог Lavazza!")
		return 9000
	case EXCELSA_CATALOG:
		fmt.Println("Это каталог Lavazza!")
		return 8000
	default:
		fmt.Printf("не смог определить каталог! Нужно пофиксить id: %s", kommInfo.OffersPackage.Id)
		return 0
	}
}

func makeSqlCommands(info *KommInfo, catalogId int) string {
	i := catalogId + 1
	resultString := ""
	resultString += fmt.Sprintf("Delete from b_catalog_price where id > %d and id < %d;\n", catalogId, catalogId+1000)
	for _, offer := range info.OffersPackage.Offers.Offer {
		for _, price := range offer.Prices.Price {
			pricePerOne, _ := strconv.Atoi(price.PricePerOne)
			if len(price.PricePerOne) > 0 {
				i += 1
				resultString += fmt.Sprintf(
					"insert into b_catalog_price values (%d, (select id from b_iblock_element where xml_id = '%s' and iblock_id=(select id from b_iblock where xml_id = '%s')), NULL, (select id from b_catalog_group where xml_id = '%s'),%d, 'RUB', CURRENT_TIMESTAMP, NULL, NULL, NULL, %d);\n",
					i,
					offer.Id,
					info.OffersPackage.Id,
					price.PriceTypeId,
					pricePerOne,
					pricePerOne,
				)
			}
		}
	}

	resultString += "UPDATE b_catalog_price t1 inner join (select s.id, s.price, s.product_id  from b_catalog_price s where product_id = (SELECT product_id from b_catalog_price where id = 999999) and CATALOG_GROUP_ID = 2 limit 1) t2 on 1=1 SET t1.price = t2.price, t1.price_scale = t2.price, t1.TIMESTAMP_X = now(), t1.product_id = t2.product_id  where t1.ID = 999999;\n"
	resultString += "UPDATE b_catalog_price t1 inner join (select s.price, s.product_id from b_catalog_price s where product_id = (SELECT product_id from b_catalog_price where id = 999999) and CATALOG_GROUP_ID = 3 limit 1) t2 ON t1.product_id=t2.product_id SET t1.price = t2.price where t1.CATALOG_GROUP_ID = 2;\n"
	return resultString
}
