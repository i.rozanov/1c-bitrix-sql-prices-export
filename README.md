# Скачать
[Перейти к релизам](https://gitlab.com/i.rozanov/1c-bitrix-sql-prices-export/-/releases)

# Собираем релиз
```shell
GOOS=windows GOARCH=amd64 go build -o bin/1c-bitrix-sql-prices-export.exe
GOOS=windows GOARCH=386 go build -o bin/1c-bitrix-sql-prices-export.exe
GOOS=linux GOARCH=amd64 go build -o bin/1c-bitrix-sql-prices-export
GOOS=darwin GOARCH=amd64 go build -o bin/1c-bitrix-sql-prices-export-darwin
```
